function save() {
  // funcion para crear objetos
  function Person(name, age) {
    //   declaramos los atributos que tendra el objeto
    this.nombre = name;
    this.edad = age;
  }
  //   se guardan los valores ingresados en los input en variables
  let nameInput = document.getElementById("name").value;
  let ageInput = parseInt(document.getElementById("age").value);
  //  instanciamos un nuevo objeto y llamamos a la funcion que crea el objeto, le pasamos como parametros las variables donde se guardaron los valores ingresados
  let newPerson = new Person(nameInput, ageInput);
  //   llamamos a la funcion que agrega el objeto a un array por ende le pasamos el objeto creado como parametro
  Add(newPerson);
}
// declaramos un array para guardar los objetos creados
let dataBase = [];
// funcion que agrega el objeto al array
function Add(person) {
  // se agrega el nuevo objeto al array
  dataBase.push(person);
  //   condicional que imprime solo usuarios mayores de 15 años
  if (person.edad >= 15) {
    //   instruccion que agrega codigo html al documento
    document.getElementById("table").innerHTML +=
      " <tbody><td>" +
      person.nombre +
      "</td><td>" +
      person.edad +
      "</td></tbody>";
  }

  console.log(dataBase);
  console.log("Los elementos en la base de datos son: " + dataBase.length);
}
