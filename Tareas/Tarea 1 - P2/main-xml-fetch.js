var contenidoXMLfetch = document.querySelector("#contenido-xml-fetch");
var selectorXMLfetch = document.getElementById("select-xml-fetch");

function convertir() {
  fetch("estudiantes.xml")
    .then((res) => res.text())
    .then((datosx) => {
      let parserXML = new DOMParser();
      let xmlDOM = parserXML.parseFromString(datosx, "application/xml");
      let estudiantes = xmlDOM.querySelectorAll("estudiante");
    });
  return estudiantes;
}

for (let valor of convertir()) {
  let option = document.createElement("option");
  option.value = valor.children[0].textContent;
  option.innerText = valor.children[0].textContent;
  selectorXMLfetch.appendChild(option);
}

function mostrarXMLfetch(apellidoXMLfetch) {
  contenidoXMLfetch.innerHTML = "";
  for (let valor of convertir()) {
    if (apellidoXMLfetch.value == valor.children[0].textContent) {
      contenidoXMLfetch.innerHTML += `
  
                          <tr>
                              <td scope="row">${valor.children[0].textContent}</td>
                              <td>${valor.children[1].textContent}</td>
                              <td>${valor.children[2].textContent}</td>
                              <td>${valor.children[3].textContent}</td>
                              <td>${valor.children[4].textContent}</td>
                              <td>${valor.children[5].textContent}</td>
                              <td>${valor.children[6].textContent}</td>
                          </tr>
  
                          `;
    }
  }
}
